import { SelectNoStyles, styles, OptionsBase } from '../Select';

export interface OccupancyOptions extends OptionsBase {
    Primary: string;
    Secondary: string;
    Investment: string;
}


export class SelectOccupancyNoStyles extends SelectNoStyles<OccupancyOptions>  {
    
}

export const SelectOccupancy = styles(SelectOccupancyNoStyles);


