import * as React from "react";
import Grid from "@material-ui/core/Grid";
import Hidden from "@material-ui/core/Hidden";
import Paper from "@material-ui/core/Paper";
import { withStyles, Theme, WithStyles } from '@material-ui/core/styles';
import { TextField, TextFieldProps, TextFieldActions } from '../TextField';
// import { Select } from '../Select';
import { Button } from '../Button';
import { Table, HeaderData, TableBodyRow } from '../Table';
import { SelectOccupancy } from '../SelectOccupancy';
import { SelectPropertyType } from '../SelectPropertyType';
import { CircularProgress } from '../CircularProgress';

type HTML_Event = (React.ChangeEventHandler<HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement>);

const headers: HeaderData[] = [
    {
        children: 'Lender',
    },
    {
        children: 'Product',
    },
    {
        children: 'Rate',
        numeric: true
    },
    {
        children: 'Closing Costs',
        numeric: true
    },
    {
        children: <Hidden xsDown>Monthly Payment</Hidden>,
        numeric: true
    },
    {
        children: <Hidden xsDown>APR</Hidden>,
        numeric: true
    }
]

export const rateQuotesLabels = {
    loanSize: 'Loan Size',
    creditScore: 'Credit Score',
    propertyType: 'Property Type',
    occupancy: 'Occupancy',
    quoteRates: 'Quote Rates',
    options: {
        propertyType: {
            SingleFamily: 'Single Family',
            Condo: 'Condo',
            Townhouse: 'Townhouse',
            MultiFamily: 'Multifamily'
        },
        occupancy: {
            Primary: 'Primary',
            Secondary: 'Secondary',
            Investment: 'Investment'
        }
    }
}

export interface TextFieldControl extends TextFieldProps, TextFieldActions {
}

export interface RateQuotesProps {
    labels: typeof rateQuotesLabels;
    rows: TableBodyRow[];
    headersControlState: {
        loanSize: {
            value: string;
        },
        creditScore: {
            value: string;
        },
        propertyType: {
            value: string;
        },
        occupancy: {
            value: string;
        }
    };
    loading: boolean;
}

export interface RateQuotesActions {
    headersControlActions: {
        loanSize: {
            onChange: HTML_Event
        },
        creditScore: {
            onChange: HTML_Event
        },
        propertyType: {
            onChange: HTML_Event
        },
        occupancy: {
            onChange: HTML_Event
        }
    };
    queryRateQuotes: {
        onClick: (event: React.MouseEvent<HTMLInputElement>) => void;
    }
}

type Classes = 'root' | 'paper' | 'quoteRatesButton';

const styles = withStyles<Classes, WithStyles<Classes>>((theme: Theme) => ({
    root: {
        flexGrow: 1,
        width: '100%'
    },
    paper: {
        padding: theme.spacing.unit * 2,
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    quoteRatesButton: {
        margin: theme.spacing.unit,
        minWidth: 120
    }
}));

export class RateQuotesNoStyles extends React.Component<RateQuotesProps & RateQuotesActions & WithStyles<Classes>> {

    render() {

        const { labels, classes, rows, headersControlState, headersControlActions, queryRateQuotes, loading } = this.props;
        // const selectPropertyType = <Select labels={{ inputLabel: labels.propertyType }} {...headersControlState.propertyType} {...headersControlActions.propertyType} />
        const selectOccupancyProps = {
            labels: {
                inputLabel: labels.occupancy,
                options: labels.options.occupancy
            },
            options: {
                Primary: 'Primary',
                Secondary: 'Secondary',
                Investment: 'Investment'
            },
            ...headersControlState.occupancy,
            ...headersControlActions.occupancy
        };
        const selectOccupancy = <SelectOccupancy {...selectOccupancyProps} />

        const selectPropertyTypeProps = {
            labels: {
                inputLabel: labels.propertyType,
                options: labels.options.propertyType
            },
            options: {
                SingleFamily: 'SingleFamily',
                Condo: 'Condo',
                Townhouse: 'Townhouse',
                MultiFamily: 'Multifamily'
            },
            ...headersControlState.propertyType,
            ...headersControlActions.propertyType
        };
        const selectPropertyType =  <SelectPropertyType {...selectPropertyTypeProps} />

        const tableOrLoading = loading ? <CircularProgress/> : <Table headers={headers} rows={rows}></Table>
        return (
            <div className={classes.root}>
                <Grid container spacing={24}>
                    <Grid item md={12} xs={12}>
                        <Paper className={classes.paper}>
                            <Grid container spacing={24}>
                                <Grid item md={6} xs={12}>
                                    <TextField label={labels.loanSize} {...headersControlState.loanSize} {...headersControlActions.loanSize} />
                                </Grid>
                                <Grid item md={6} xs={12}>
                                    <TextField label={labels.creditScore} {...headersControlState.creditScore} {...headersControlActions.creditScore} />
                                </Grid>
                                <Grid item md={6} xs={12}>
                                    {selectPropertyType}
                                </Grid>
                                <Grid item md={6} xs={12}>
                                    {selectOccupancy}
                                </Grid>
                                <Grid item md={6} xs={6}>
                                </Grid>
                                <Grid item md={6} xs={6}>
                                    <Button className={classes.quoteRatesButton} {...queryRateQuotes}>{labels.quoteRates}</Button>
                                </Grid>
                            </Grid>
                        </Paper>
                    </Grid>
                    <Grid item md={12} xs={12}>
                        <Paper className={classes.paper}>
                            {tableOrLoading}
                        </Paper>
                    </Grid>
                </Grid>
            </div>
        );
    }
}

export const RateQuotes = styles(RateQuotesNoStyles);
