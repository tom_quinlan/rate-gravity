import * as React from "react";
import MaterialUiCircularProgress from "@material-ui/core/CircularProgress";
import * as _ from "lodash";

export class CircularProgress extends React.Component<{}, {completed: number;}> {
    timer: null | NodeJS.Timer = null;

    state = {
      completed: 0,
    };

    componentDidMount() {
        this.timer = setInterval(this.progress, 20);
      }
    
      componentWillUnmount() {
        clearInterval(this.timer as NodeJS.Timer);
      }
    
      progress = () => {
        const { completed } = this.state;
        this.setState({ completed: completed >= 100 ? 0 : completed + 1 });
      };

    render() {
        return (
            <MaterialUiCircularProgress           
            variant="determinate"
            size={100}
            value={this.state.completed} 
            />
        );
    }
}
