import * as React from "react";
import MaterialUiButton, { ButtonProps as MaterialUiButtonProps} from "@material-ui/core/Button";
import { withStyles, Theme, WithStyles } from '@material-ui/core/styles';
import * as _ from "lodash";
import { separateStylesProps } from '../utils';


export interface Props extends MaterialUiButtonProps {

}

export interface Actions {
}

type Classes = 'button';

const styles = withStyles<Classes, WithStyles<Classes>>((theme: Theme) => ({
    button: {

    }
}));

class ButtonLabelNoStyles extends React.Component<Props & Actions & WithStyles<Classes>> {

    render() {
        const { props } = separateStylesProps(this.props);
        return (
            <MaterialUiButton {...props}>{this.props.children}</MaterialUiButton>
        );
    }
}

export const Button = styles(ButtonLabelNoStyles);