import * as React from 'react';
import { shallow, configure } from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-16';
import { InputLabel } from '.';

configure({ adapter: new Adapter() });

it('ButtonLabel should render when given valid props', () => {

    const result = shallow(<InputLabel> Test Label </InputLabel>);
    expect(result).toMatchSnapshot();
});
