import * as React from "react";
import MaterialUiInputLabel, { InputLabelProps as MaterialUiInputProps } from "@material-ui/core/InputLabel";
import { withStyles, Theme, WithStyles } from '@material-ui/core/styles';
import { separateStylesProps } from '../utils';

export interface InputProps extends MaterialUiInputProps {

}

export interface InputActions {

}

type Classes = 'inputLabel';

const styles = withStyles((theme: Theme) => ({
    inputLabel: {
        
    }
}));


class SelectNoStyles extends React.Component<InputProps & InputActions & WithStyles<Classes>> {

    render() {
        const { props, styles } = separateStylesProps(this.props);
        return (
        <MaterialUiInputLabel
          {...props}
          className={styles.inputLabel}
        >{this.props.children}
        </MaterialUiInputLabel>
        );
    }
}

export const InputLabel = styles(SelectNoStyles);

