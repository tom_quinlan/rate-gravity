import * as React from "react";
import MaterialUiTextField, { TextFieldProps as MaterialUiTextFieldProps } from "@material-ui/core/TextField";
import { withStyles, Theme, WithStyles } from '@material-ui/core/styles';
import { separateStylesProps } from '../utils';

export interface TextFieldProps extends MaterialUiTextFieldProps{
}

export interface TextFieldActions {

}

type Classes = 'textField';

const styles = withStyles<Classes, WithStyles<Classes>>((theme: Theme) => ({
      textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 200,
      }
}));


class TextFieldNoStyles extends React.Component<TextFieldProps & TextFieldActions & WithStyles<Classes>> {

    render() {
        const { props, styles } = separateStylesProps(this.props);
        return (
        <MaterialUiTextField
          {...props}
          className={styles.textField}
        />
        );
    }
}

export const TextField = styles(TextFieldNoStyles);
