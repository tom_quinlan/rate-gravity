import { SelectNoStyles, styles, OptionsBase } from '../Select';

export interface PropertyTypeOptions extends OptionsBase {
    SingleFamily: string;
    Condo: string;
    Townhouse: string;
    MultiFamily: string;
}


export class SelectPropertyTypeNoStyles extends SelectNoStyles<PropertyTypeOptions>  {
    
}

export const SelectPropertyType = styles(SelectPropertyTypeNoStyles);


