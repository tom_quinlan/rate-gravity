import * as _ from "lodash";

interface PropsWithClasses {
    [key: string]: any;
}

type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;

interface StylesProps<StylesPropsInstances extends any> {
    props: Omit<StylesPropsInstances, 'classes'>;
    styles: StylesPropsInstances['classes'];
}

// Separates material ui classes, passed in by withStyles, from the rest of the Component's props
export function separateStylesProps<SeparateStylesProps extends PropsWithClasses>(stylesProps: SeparateStylesProps) {
    const props = _.omit(stylesProps, 'classes');
    const styles = stylesProps.classes;
    return {
        props,
        styles
    } as StylesProps<SeparateStylesProps>;
}
