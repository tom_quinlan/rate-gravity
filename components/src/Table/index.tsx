import MaterialUiTable from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import MaterialUiTableRow, { TableRowProps as MaterialUiTableRowProps } from '@material-ui/core/TableRow';
import MaterialUiTableCell, { TableCellProps as MaterialUiTableCellProps } from '@material-ui/core/TableCell';
import MaterialUiTableBody from '@material-ui/core/TableBody';
import * as React from "react";

export interface HeaderData extends MaterialUiTableCellProps {

}

interface TableProps {
    headers: HeaderData[];
    rows: TableBodyRow[];
}

export interface HeadersProps {
    headers: HeaderData[];
}

function Headers({ headers }: HeadersProps) {
    const headersJsx = headers.map((header, key) => {
        return (
            <MaterialUiTableCell key={key} {...header} />
        );
    });
    return (
        <React.Fragment>
        {headersJsx}
        </React.Fragment>
    );
}

export interface TableCell extends MaterialUiTableCellProps {

}

export interface TableBodyRow extends MaterialUiTableRowProps {
    cells: TableCell[];
}

export interface TableBodyProps {
    rows: TableBodyRow[];
}

function TableBody({ rows }: TableBodyProps) {
    const rowsJsx = rows.map((row, key) => {
        const cellsJsx = row.cells.map((cell, key) => <MaterialUiTableCell key={key} {...cell} /> );
        return (
            <MaterialUiTableRow key={key} {...row}>
                {cellsJsx}
            </MaterialUiTableRow>
        );
    });
    return (
        <MaterialUiTableBody>
        {rowsJsx}
        </MaterialUiTableBody>
    );
}

export function Table({ headers, rows }: TableProps) {
    return (
        <MaterialUiTable>
            <TableHead>
                <MaterialUiTableRow>
                    <Headers headers={headers} />
                </MaterialUiTableRow>
            </TableHead>
            <TableBody rows={rows} />
        </MaterialUiTable>
    );
}