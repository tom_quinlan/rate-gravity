import * as React from "react";
import MaterialUiSelect, { SelectProps as MaterialUiSelectProps } from "@material-ui/core/Select";
import FormControl from '@material-ui/core/FormControl';
import MenuItem from '@material-ui/core/MenuItem';
import { withStyles, Theme, WithStyles } from '@material-ui/core/styles';
import { InputLabel } from '../InputLabel';
import { separateStylesProps } from '../utils';
import * as _ from "lodash";

export interface SelectProps<Options> extends MaterialUiSelectProps {
    labels: {
        inputLabel: string;
        options: {
            [P in keyof Options]: string;
        } 
    }
    options: Options;
}

export interface SelectActions {

}

type Classes = 'formControl';

export const styles = withStyles<Classes, WithStyles<Classes>>((theme: Theme) => ({
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 120,
    }
}));

export interface OptionsBase {
    [key: string]: string;
}

export class SelectNoStyles<Options extends OptionsBase> extends React.Component<SelectProps<Options> & SelectActions & WithStyles<Classes>> {

    render() {
        const { props, styles } = separateStylesProps(this.props);
        const menuItems = _.map(Object.keys(props.options), option => {
            return (<MenuItem value={option}>{props.labels.options[option]}</MenuItem>)
        })
        return (
            <FormControl className={styles.formControl}>
                <InputLabel htmlFor="age-simple">{props.labels.inputLabel}</InputLabel>
                <MaterialUiSelect {...props}
                >   
                    {menuItems}
                </MaterialUiSelect>
            </FormControl>
        );
    }
}

