import * as React from "react";
import { Button } from "@material-ui/core";

export interface Props {
    label: string;
}

interface ChangeLabelArgs {
    readonly value: string;
}

export interface Actions {
    changeLabel: (args: ChangeLabelArgs) => void;
}

export default class ButtonLabel extends React.Component<Props & Actions> {

    onButtonLabelClick() {
        this.props.changeLabel({value: 'newLabel'});
    }

    render() {
        return (
            <Button onClick={this.onButtonLabelClick.bind(this)}>{this.props.label}</Button>
        );
    }
}
