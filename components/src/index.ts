import ButtonLabel, { Props as ButtonLabelProps, Actions as ButtonLabelActions} from './ButtonLabel';
export * from './TextField';
export * from './RateQuotes';
export * from './SelectOccupancy';

function sample(params: string) {
    return params;
}

export { ButtonLabel, ButtonLabelProps as ButtonLabelProps, ButtonLabelActions as  ButtonLabelActions};

export default sample;
