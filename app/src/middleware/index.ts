import saveToLocalStorageState from './saveToLocalStorageState';
import thunk from 'redux-thunk';

export default [
    saveToLocalStorageState,
    thunk
];
