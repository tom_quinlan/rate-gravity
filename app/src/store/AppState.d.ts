import { rateQuotesLabels } from 'components';
import { ThunkDispatch } from 'redux-thunk';
import { AnyAction } from 'redux';

export interface Label {
    label: string;
}

export interface RateQuotesQuery {
    loanSize: string | null;
    creditScore: string | null;
    propertyType: string | null;
    occupancy: string | null;
}

interface RateRow {
    lenderName: string; 
    loanType: string;
    interestRate: number;
    closingCosts: number; 
    monthlyPayment: number; 
    apr: number; 
}

export interface RateQuotes {
    rows: RateRow[];
    query: RateQuotesQuery;
    loading: boolean;
}

export interface ResourceLabels {
    rateQuotes: typeof rateQuotesLabels;
}

export type AppStateThunkDispatch = ThunkDispatch<AppState, void, AnyAction>;

export default interface AppState {
    label: Label;
    rateQuotes: RateQuotes;
    resourceLabels: ResourceLabels;
}