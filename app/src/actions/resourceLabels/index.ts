import simpleActions from './simpleActions';
import { ResourceLabels } from '../../store/AppState';
import { createReducer, getCreators as getActions } from 'tquinlan92-typescript-redux-utils';
import { rateQuotesLabels } from 'components';

const initialState = {
    rateQuotes: rateQuotesLabels
};

const namedActionReducers = simpleActions('RateQuotes');

export default {
    reducer: createReducer<ResourceLabels>(initialState, namedActionReducers),
    actions: {
        ...getActions(namedActionReducers),
    }
};
