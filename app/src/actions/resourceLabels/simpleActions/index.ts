import updateResourceLabels from "./updateResourceLabels";

export default (reducerName: string) => {
    return {
        updateResourceLabels: updateResourceLabels(reducerName)
    };
};
