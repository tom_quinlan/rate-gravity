import { ResourceLabels } from "../../../store/AppState";
import { makeActionCreatorWithReducerWithPrefix } from "tquinlan92-typescript-redux-utils";

export interface OnTagSelectionChangeAction {
    resourceLabels: ResourceLabels;
}

export default makeActionCreatorWithReducerWithPrefix<ResourceLabels, OnTagSelectionChangeAction>(
    'UPDATE-RESOURCE-LABELS',
    (state, { resourceLabels }): ResourceLabels => {
        return resourceLabels;
    }
);
