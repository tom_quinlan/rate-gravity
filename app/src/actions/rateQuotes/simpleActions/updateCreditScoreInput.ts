import { createUpdateQueryAction } from './utils';

export default createUpdateQueryAction({type: 'UPDATE-CREDIT-SCORE-INPUT', key: 'creditScore'});
