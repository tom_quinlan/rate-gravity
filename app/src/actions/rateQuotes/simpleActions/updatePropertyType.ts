import { createUpdateQueryAction } from './utils';

export default createUpdateQueryAction({type: 'PROPERTY-TYPE', key: 'propertyType'});
