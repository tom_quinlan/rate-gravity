import { RateQuotes } from "../../../store/AppState";
import { makeActionCreatorWithReducerWithPrefix } from "tquinlan92-typescript-redux-utils";

export interface UpdateLoadingParams {
    value: boolean;
}

export default makeActionCreatorWithReducerWithPrefix<RateQuotes, UpdateLoadingParams>(
    'UPDATE-LOADING',
    (state, { value }): RateQuotes => {
        return {
            ...state,
            loading: value
        };
    }
);
