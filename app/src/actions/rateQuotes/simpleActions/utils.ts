import { makeActionCreatorWithReducerWithPrefix } from "tquinlan92-typescript-redux-utils";
import { RateQuotes, RateQuotesQuery } from "../../../store/AppState";
import * as _ from "lodash";

type CreateUpdateActionKey = keyof RateQuotesQuery;

interface CreateUpdateQueryAction {
    type: string;
    key: CreateUpdateActionKey;
}

interface UpdateActionParams {
    value: string;
}

export function createUpdateQueryAction({type, key}: CreateUpdateQueryAction) {
    return makeActionCreatorWithReducerWithPrefix<RateQuotes, UpdateActionParams>(
        `UPDATE-${type}`,
        (state, { value }): RateQuotes => {
            const newQueryState = _.clone(state.query);
            newQueryState[key] = value;
            return {
                ...state,
                query: {
                    ...newQueryState
                }
            };
        }
    );
}
