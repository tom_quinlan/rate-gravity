import { createUpdateQueryAction } from './utils';

export default createUpdateQueryAction({type: 'OCCUPANCY', key: 'occupancy'});
