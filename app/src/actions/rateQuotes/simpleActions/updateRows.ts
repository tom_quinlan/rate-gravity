import { RateQuotes, RateRow } from "../../../store/AppState";
import { makeActionCreatorWithReducerWithPrefix } from "tquinlan92-typescript-redux-utils";

export interface UpdateRowsParams {
    rows: RateRow[];
}

export default makeActionCreatorWithReducerWithPrefix<RateQuotes, UpdateRowsParams>(
    'UPDATE-ROWS',
    (state, { rows }): RateQuotes => {
        return {
            ...state,
            rows
        };
    }
);
