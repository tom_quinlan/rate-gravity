import updateRows from "./updateRows";
import updateLoanSizeInput from "./updateLoanSizeInput";
import updateCreditScoreInput from './updateCreditScoreInput';
import updatePropertyType from './updatePropertyType';
import updateOccupancy from './updateOccupancy';
import updateLoading from './updateLoading';

export default (reducerName: string) => {
    return {
        updateRows: updateRows(reducerName),
        updateLoanSizeInput: updateLoanSizeInput(reducerName),
        updateCreditScoreInput: updateCreditScoreInput(reducerName),
        updatePropertyType: updatePropertyType(reducerName),
        updateOccupancy: updateOccupancy(reducerName),
        updateLoading: updateLoading(reducerName)
    };
};
