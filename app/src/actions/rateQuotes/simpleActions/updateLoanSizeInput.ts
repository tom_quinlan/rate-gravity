import { createUpdateQueryAction } from './utils';

export default createUpdateQueryAction({type: 'LOAN-SIZE-INPUT', key: 'loanSize'});
