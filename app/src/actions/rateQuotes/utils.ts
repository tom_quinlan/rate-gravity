interface RateRow {
    lenderName: string; 
    loanType: string;
    interestRate: number;
    closingCosts: number; 
    monthlyPayment: number; 
    apr: number; 
}
function createChildrenElement(children: string) {
    return {
        children
    };
}

function convertRateToRow(row: RateRow) {
    return {
        cells: [
            createChildrenElement(row.lenderName),
            createChildrenElement(row.loanType),
            {
                ...createChildrenElement(String(row.interestRate)), numeric: true
            },
            {
                ...createChildrenElement(String(row.closingCosts)), numeric: true
            },
            {
                ...createChildrenElement(String(row.monthlyPayment)), numeric: true
            },
            {
                ...createChildrenElement(String(row.apr)), numeric: true
            }
        ]
    };
}

export function convertRatesToRows(rows: RateRow[]) {
    return rows.map(row => convertRateToRow(row));
}
