import { ThunkAction } from "redux-thunk";
import AppState from "../../../store/AppState";
import { AnyAction } from "redux";
import * as request from "superagent";
import actions from "../..";

const getApiKey = new Promise<string>((resolve, reject) => {
    request
        .get('/static/api.json')
        .end((err, res) => {
            if (err) {
                reject(err);
            } else {
                console.log('res', res);
                resolve(res.body.apiKey);
            }
        });
});

interface Query {
    creditScore: number | null;
    loanSize: number | null;
    occupancy: string | null;
    propertyType: string | null;
}

interface Request {
    apiKey: string;
}

interface GetRequestIdParams extends Request {
    query: Query;
}

function getRequestId({ query, apiKey }: GetRequestIdParams) {
    return new Promise<string>((resolve, reject) => {
        request
            .post('https://ss6b2ke2ca.execute-api.us-east-1.amazonaws.com/Prod/ratequotes')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(query))
            .set('Authorization', `RG-AUTH ${apiKey}`)
            .end((err, res) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(res.body.requestId);
                }
            });
    });
}

interface GetResultsFromRequestIdParams extends Request {
    requestId: string;
}

function getResultsFromRequestId({ requestId, apiKey }: GetResultsFromRequestIdParams) {
    return new Promise<any>((resolve, reject) => {
        function getResults() {
            request
            .get('https://ss6b2ke2ca.execute-api.us-east-1.amazonaws.com/Prod/ratequotes')
            .query({ requestId })
            .set('Authorization', `RG-AUTH ${apiKey}`)
            .end((err, res) => {
                if (err) {
                    reject(err);
                } else {
                    if (!res.body.done) {
                        setTimeout(() => { getResults(); }, 3000);
                    } else {
                        resolve(res.body);
                    }
                }
            });
        }
        getResults();
    });
}

export default function (): ThunkAction<void, AppState, void, AnyAction> {
    return async function (dispatch, getState) {
        dispatch(actions.rateQuotes.updateLoading({value: true}));
        const state = getState();
        const apiKey = await getApiKey;
        const rateQuotesQuery = {
            ...state.rateQuotes.query,
            creditScore: Number(state.rateQuotes.query.creditScore),
            loanSize: Number(state.rateQuotes.query.loanSize)
        };
        const requestId = await getRequestId({ query: rateQuotesQuery, apiKey });
        console.log('requestId', requestId);
        const queryResults = await getResultsFromRequestId({ apiKey, requestId });
        dispatch(actions.rateQuotes.updateRows({rows: queryResults.rateQuotes}));
        dispatch(actions.rateQuotes.updateLoading({value: false}));
    };
}
