import simpleActions from './simpleActions';
import { RateQuotes } from '../../store/AppState';
import { createReducer, getCreators as getActions } from 'tquinlan92-typescript-redux-utils';
import thunkActions from './thunkActions';

const initialState = {
    rows: [],
    query: {
        loanSize: null,
        creditScore: null,
        propertyType: null,
        occupancy: null
    },
    loading: false
};

const namedActionReducers = simpleActions('RateQuotes');

export default {
    reducer: createReducer<RateQuotes>(initialState, namedActionReducers),
    actions: {
        ...getActions(namedActionReducers),
        ...thunkActions
    }
};
