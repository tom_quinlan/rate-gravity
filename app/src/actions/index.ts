import { combineReducers, AnyAction } from 'redux';
export { AnyAction };
import AppState from '../store/AppState';
import label from './label';
import rateQuotes from './rateQuotes';
import resourceLabels from './resourceLabels';
import { getActionsAndReducersFromCombinedActionReducer } from 'tquinlan92-typescript-redux-utils';

const actionsReducersTree = {
    label,
    rateQuotes,
    resourceLabels
};

const { actions, reducers } = getActionsAndReducersFromCombinedActionReducer(actionsReducersTree);

export const rootReducer = combineReducers<AppState>(reducers);

export default actions;
