import AppState, { AppStateThunkDispatch } from "../../store/AppState";
import { connect } from "react-redux";
import { RateQuotes, RateQuotesProps, RateQuotesActions } from 'components';
import actions from '../../actions';
import { Dispatch } from "redux";
import { convertRatesToRows } from "../../actions/rateQuotes/utils";

type HTML_Event = React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement>;

const mapStateToProps = ({ rateQuotes, resourceLabels }: AppState): RateQuotesProps => {
    return {
        labels: resourceLabels.rateQuotes,
        rows: convertRatesToRows(rateQuotes.rows),
        headersControlState: {
            loanSize: {
                value: rateQuotes.query.loanSize ? rateQuotes.query.loanSize : ''
            },
            creditScore: {
                value: rateQuotes.query.creditScore ? rateQuotes.query.creditScore : ''
            },
            propertyType: {
                value: rateQuotes.query.propertyType ? rateQuotes.query.propertyType : ''
            },
            occupancy: {
                value: rateQuotes.query.occupancy ? rateQuotes.query.occupancy : ''
            }
        },
        loading: rateQuotes.loading
    };
};

function mapDispatchToProps(dispatch: Dispatch): RateQuotesActions {
    return {
        headersControlActions: {
            loanSize: {
                onChange: (newValue: HTML_Event) => {
                    dispatch(actions.rateQuotes.updateLoanSizeInput({value: newValue.target.value}));
                }
            },
            creditScore: {
                onChange: (newValue: HTML_Event) => {
                    dispatch(actions.rateQuotes.updateCreditScoreInput({value: newValue.target.value}));
                }
            },
            propertyType: {
                onChange: (newValue: HTML_Event) => {
                    console.log('newValue', newValue);
                    dispatch(actions.rateQuotes.updatePropertyType({value: newValue.target.value}));
                }
            },
            occupancy: {
                onChange: (newValue: HTML_Event) => {
                    console.log('newValue', newValue);
                    dispatch(actions.rateQuotes.updateOccupancy({value: newValue.target.value}));
                }
            }
        },
        queryRateQuotes: {
            onClick: (newValue: React.MouseEvent<HTMLInputElement>) => {
                console.log('newValue', newValue);
                (dispatch as AppStateThunkDispatch)(actions.rateQuotes.queryRateQuotes());
            }
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(RateQuotes);

