import actions from '../../actions';
import AppState from "../../store/AppState";
import { connect } from "react-redux";
import { ButtonLabel, ButtonLabelProps, ButtonLabelActions } from 'components';


const mapStateToProps = ({ label }: AppState) => {
    return label;
};

const mapDispatchToProps = {
    ...actions.label
};

export default connect<ButtonLabelProps, ButtonLabelActions>(mapStateToProps, mapDispatchToProps)(ButtonLabel);
