import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import Theme from '../Theme';
import RateQuotes from '../RateQuotes';

const style = {
    margin: '5%'
}

export default (store: any) => {
    return ReactDOM.render(
        <Provider store={store}>
            <Theme>
                <div style={style}>
                <RateQuotes />
                </div>
            </Theme>
        </Provider>
        , document.getElementById('app')
    );
};
