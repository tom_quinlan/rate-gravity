[![CircleCI](https://circleci.com/bb/tom_quinlan/rate-gravity.svg?style=svg&circle-token=8ac3f00e1831ea18c96a0f9de2777fa54abc7d4a)](https://circleci.com/bb/tom_quinlan/rate-gravity)

![Alt Text](./demo.gif)

# Architecture of monorepo npm packages (I should have used https://lernajs.io/)
There are 4 npm packages in total in this repository.  They include the `root package.json`, `components/package.json`, `app/package.json`, and `storybook/package.json`.  

`components/package.json` outputs a library of compenents that extend @material-ui/core components. `app/package.json` and `storybook/package.json` are dependent on `components/package.json`.  

`app/package.json` contains a functioning application using redux to wire the components from `components/package.json`.  

`storybook/package.json` is an application demoing how different states given to components from `components/package.json` render.  

The `root package.json` contains npm scripts to help install, develop, test, build across all 3 projects  

## Installation
- Upgrade to the latest npm (use ^6.0.0)
    - `npm install npm@latest -g`
- Install dependencies
    - `npm ci`

## Development
- To start the development environment for `app`
    - `npm start`
- To start the development environment for `storybook`
    - `npm run start-storybook`

## Testing 
- To test all projects (`components`, `app`, `storybook`)
    - `npm run test-all`

## Build
- To build all projects (`components`, `app`, `storybook`)
    - `npm run build-all`



### Personal Takeaways from project
- Have found https://lernajs.io/ since starting this project and it would have helped on many occasions where I've had dependency version mismatches.  There's a feature in https://lernajs.io/ to hoist common node modules to the root and let's you know what version can be compatible with all packages.
- Typescript definitely helps when working in a team, but sometimes something that is very simple in vanilla javascript can be very tricky to set up with types in Typescript.  For instance it took me a little bit to come up with a design to re-use a Select Component and I'm still not super proud of the design I came up with.
- I love tests for so many different reasons, but if I decided this code was throw away work I could have spent less time setting up the test/jest framework in the packages.  I hope to build on some of the components I've developed in the project still though.
- Also setting up https://storybook.js.org/ took some time and it was definitely unnecessary for this small project.  Though it was a UI development tool I found recently that I thought I'd share by setting up in this project.