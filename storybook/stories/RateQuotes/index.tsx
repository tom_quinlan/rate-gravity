import * as React from 'react';
import { storiesOf } from '@storybook/react';
import { RateQuotes, rateQuotesLabels } from 'components';

const style = {
    padding: '5%'
}

type HTML_Event = React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement>;

const props = {
    labels: rateQuotesLabels,
    rows: [],
    headersControlState: {
        loanSize: {
            value: ''
        },
        creditScore: {
            value: ''
        },
        propertyType: {
            value: ''
        },
        occupancy: {
            value: ''
        }
    },
    loading: false,
    headersControlActions: {
        loanSize: {
            onChange: (newValue: HTML_Event) => {
            }
        },
        creditScore: {
            onChange: (newValue: HTML_Event) => {
            }
        },
        propertyType: {
            onChange: (newValue: HTML_Event) => {
            }
        },
        occupancy: {
            onChange: (newValue: HTML_Event) => {
            }
        }
    },
    queryRateQuotes: {
        onClick: (newValue: React.MouseEvent<HTMLInputElement>) => {
        }
    }
}

storiesOf('Rates Quotes', module)
    .add('case 1', () => (
        <div style={style}>
            <RateQuotes {...props}/>
        </div>
    ))